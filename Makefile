all: dist/zsh dist/vim dist/tmux

dist/zsh: $(foreach f, $(filter zsh/_%, $(shell echo zsh/*)), link-dot-file-$(subst zsh/,zsh-,$(f))) $(HOME)/.zshrc.local
	echo 'zsh'

dist/tmux: $(foreach f, $(filter tmux/_%, $(shell echo tmux/*)), link-dot-file-$(subst tmux/,tmux-,$(f)))
	echo 'tmux'

dist/vim: $(foreach f, $(filter vim/_%, $(shell echo vim/*)), link-dot-file-$(subst vim/,vim-,$(f))) $(HOME)/.vim/tmp $(HOME)/.vimrc.local
	vim +NeoBundleInstall +q

link-dot-file-vim-%: vim/%
	ln -snf $(CURDIR)/$< $(HOME)/$(subst vim/_,.,$(<))

link-dot-file-tmux-%: tmux/%
	ln -snf $(CURDIR)/$< $(HOME)/$(subst tmux/_,.,$(<))

link-dot-file-zsh-%: zsh/%
	ln -snf $(CURDIR)/$< $(HOME)/$(subst zsh/_,.,$(<))

$(HOME)/.zshrc.local:
	touch $(HOME)/.zshrc.local
$(HOME)/.vimrc.local:
	touch $(HOME)/.vimrc.local
$(HOME)/.vim/tmp:
	mkdir -p $(HOME)/.vim/tmp

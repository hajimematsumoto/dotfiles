#!/bin/bash

# Dropboxをスタートします

# ホームディレクトリへインストール

cd $HOME;
wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" |\
tar zxvf -

#
# デーモンの起動
#
# 初回起動の場合リンクするためのURLが表示されるのでブラウザからアクセスする
#
($HOME/.dropbox-dist/dropboxd &) &


# hownの保存先をDropbox内部にする
# mkdir ~/Dropbox/howm
# vi .vimrc.local
# let howm_dir = '~/Dropbox/howm' <-- これを追記

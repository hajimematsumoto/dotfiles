Dotfiles
========

tool
-----
- `tool/start_dropbox.sh`

使い方
------

```sh
git clone https://hajimematsumoto@bitbucket.org/hajimematsumoto/dotfiles.git ~/.dotfiles

cd ~/.dotfiles
make all
```
